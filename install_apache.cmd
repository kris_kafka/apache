@echo off
@setlocal
if ""=="%ProjectsDir%" set ProjectsDir=%UserProfile%\Projects
if ""=="%ApacheDir%"   set ApacheDir=%ProjectsDir%

set SrcDir=%ProjectsDir%\apache
set LibDir=%ApacheDir%\lib
set IncDir=%ApacheDir%\inc
set AprInc=%ApacheDir%\inc\apr
set LogInc=%ApacheDir%\inc\log4cxx

set DelFlg=
if 01==1 set DelFlg=/y/ne

call :MakeDir %ApacheDir%
call :MakeDir %IncDir%
call :MakeDir %LibDir%
call :MakeDir %AprInc%
call :MakeDir %LogInc%

call :RmFile %LibDir%\apr-1.lib
call :RmFile %LibDir%\apr-1d.lib
call :RmFile %LibDir%\aprutil-1.lib
call :RmFile %LibDir%\aprutil-1d.lib
call :RmFile %LibDir%\xml.lib
call :RmFile %LibDir%\xmld.lib
call :RmFile %LibDir%\log4cxx.lib
call :RmFile %LibDir%\log4cxxd.lib
call :RmTree %AprInc%
call :RmTree %LogInc%

call :CopyD %SrcDir%\apr\LibD\apr-1d.lib %LibDir%
call :CopyD %SrcDir%\apr\LibR\apr-1.lib %LibDir%
call :CopyD %SrcDir%\apr-util\LibD\aprutil-1d.lib %LibDir%
call :CopyD %SrcDir%\apr-util\LibR\aprutil-1.lib %LibDir%
call :CopyD %SrcDir%\apr-util\xml\expat\lib\LibD\xmld.lib %LibDir%
call :CopyD %SrcDir%\apr-util\xml\expat\lib\LibR\xml.lib %LibDir%
call :CopyD %SrcDir%\log4cxx\projects\Debug\log4cxxd.lib %LibDir%
call :CopyD %SrcDir%\log4cxx\projects\Release\log4cxx.lib %LibDir%
call :CopyN %SrcDir%\apr\include\*.h %AprInc%\
call :CopyN %SrcDir%\apr-util\include\*.h %AprInc%\
call :CopyT %SrcDir%\log4cxx\src\main\include\log4cxx %LogInc%\

goto :EOF

:CopyD
REM echo Copying %1 to %2
%DbgEcho% if not exist %2\nul mkdir %2
%DbgEcho% xcopy /y/q %1 %2 | find /v "File(s) copied"
goto :EOF

:CopyN
%DbgEcho% xcopy /y/q %1 %2 | find /v "File(s) copied"
goto :EOF

:CopyT
%DbgEcho% xcopy /y/s/e/q %1 %2 | find /v "File(s) copied"
goto :EOF

:MakeDir
%DbgEcho% if not exist %1\nul mkdir %1
goto :EOF

:RmFile
%DbgEcho% if exist %1 del %DelFlg% /f/q %1
goto :EOF

:RmTree
%DbgEcho% if exist %1\nul del %DelFlg% /f/q/s %1\* | find /v "Deleted file -"
goto :EOF
